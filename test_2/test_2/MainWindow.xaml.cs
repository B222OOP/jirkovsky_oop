﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace test_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Room> room_list = new List<Room>();
        List<Patient> patient_list = new List<Patient>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void create_room_clicked(object sender, RoutedEventArgs e)
        {
            RoomWindow room_window = new RoomWindow();
            room_window.ShowDialog();
            room_list.Add(room_window.room);

            room_listbox.ItemsSource = room_list;
            room_listbox.Items.Refresh();
            error_label.Content = "";
        }

        private void create_patient_clicked(object sender, RoutedEventArgs e)
        {
            PatientWindow patient_window = new PatientWindow();
            patient_window.ShowDialog();
            patient_list.Add(patient_window.patient);
 
            patient_listbox.ItemsSource = patient_list;
            patient_listbox.Items.Refresh();
            error_label.Content = "";
        }

        private void room_listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Room room = (Room)room_listbox.SelectedItem;
            bed_listbox.Visibility = Visibility.Visible;
            bed_listbox.IsEnabled = false;

            bed_listbox.ItemsSource = room.bed_list;
            bed_listbox.Items.Refresh();
            bed_listbox.IsEnabled = true;
            error_label.Content = "";
            info_label.Content = "";
        }

        private void bed_listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Bed bed = (Bed)bed_listbox.SelectedItem;
            try
            {
                if (bed.occupied == true) { info_label.Content = $"Bed occupied by: {bed.occupied_by.first_name} {bed.occupied_by.last_name}";}
            }
            catch (NullReferenceException){ info_label.Content = "Bed is not occupied"; }
            error_label.Content = "";
        }
        private void patient_to_bed_button_Click(object sender, RoutedEventArgs e)
        {
            Bed bed = (Bed)bed_listbox.SelectedItem;
            Patient patient = (Patient)patient_listbox.SelectedItem;

            if (bed.occupied == false)
            {
                bed.set_occupier(patient);
                info_label.Content = $"Patient successfully added to the bed!";
            }
            else { error_label.Content = "Bed is already occupied!"; }
        }
    }
}
