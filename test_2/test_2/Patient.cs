﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_2
{
    public class Patient
    {
        public string first_name { get; private set; }
        public string last_name { get; private set; }
        public string full_name { get; private set; }
        public int id { get; private set; }
        public float weight { get; private set; }
        public float height { get; private set; }

        public Patient(string first_name, string last_name, int id, int height, int weight)
        {
            this.first_name = first_name;
            this.last_name = last_name;
            this.id = id;
            this.weight = weight;
            this.height = height;
            this.full_name = $"{first_name} {last_name}";
        }
    }
}
