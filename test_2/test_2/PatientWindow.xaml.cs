﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace test_2
{
    public partial class PatientWindow : Window
    {
        public Patient patient { get; set; }

        public PatientWindow()
        {
            InitializeComponent();
        }

        private void patient_created_button_Click(object sender, RoutedEventArgs e)
        {
            patient = new Patient(first_name_textbox.Text,
                last_name_textbox.Text,
                Int32.Parse(id_textbox.Text),
                Int32.Parse(height_textbox.Text),
                Int32.Parse(weight_textbox.Text));
            Close();
        }
    }
}
