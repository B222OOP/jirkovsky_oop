﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace test_2
{
    /// <summary>
    /// Interaction logic for RoomWindow.xaml
    /// </summary>
    public partial class RoomWindow : Window
    {
        public Room room { get; set; }

        public RoomWindow()
        {
            InitializeComponent();
        }

        public void room_created_button_Click(object sender, RoutedEventArgs e)
        {   
            this.room = new Room(
                Int32.Parse(number_of_beds_textbox.Text),
                Int32.Parse(max_weight_textbox.Text));
            Close();
        }
    }
}