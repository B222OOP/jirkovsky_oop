﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_2
{
    public class Bed
    {
        public int max_weight { get; private set; }
        public string name { get; set; }

        public bool occupied { get; set; } = false;
        public Patient occupied_by { get; set; }

        public Bed(int max_weight, int id) 
        { 
            this.max_weight = max_weight;
            this.name = "Bed_" + id.ToString();
        }

        public void set_occupier(Patient patient)
        {
            this.occupied = true;
            this.occupied_by = patient;
        }
    }
}
