﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_2
{
    public class Room
    {
        public int number_of_beds { get; private set; }
        public int max_weight { get; private set; }
        public string name { get; private set; }

        public List<Bed> bed_list { get; private set; } = new List<Bed>();

        public Room(int number_of_beds, int max_weight)
        {
            this.number_of_beds = number_of_beds;
            this.max_weight = max_weight;
            this.name = $"Room_{max_weight}";

            for (int i = 1; i <= this.number_of_beds; i++) this.bed_list.Add(new Bed(this.max_weight, i));
        }
    }
}