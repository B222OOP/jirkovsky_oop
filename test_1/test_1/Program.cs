﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace test_1
{
    internal class Program
    {
        public static List<Patient> patient_list = new List<Patient>();
        public static List<Room> room_list = new List<Room>();

        public static void Main(string[] args)
        {   
            patient_list.Add(new Patient("Adam", "Mojzis", 0123456789, 178, 120));
            patient_list.Add(new Patient("Patrik", "Dvorak", 0123456788, 182, 150));
            patient_list.Add(new Patient("Josef", "Sezemsky", 0123456777, 170, 100));
            patient_list.Add(new Patient("Jakub", "Hauser", 0123456666, 180, 115));

            room_list.Add(new Room(4, 130));
            room_list.Add(new Room(1, 175));
            room_list.Add(new Room(2, 110));

            // "Patients" for testing
            Patient patient_over_limit = new Patient("Mistni", "Jouda", 1111111111, 185, 220);
            Patient healthy_person = new Patient("Cristiano", "Ronaldo", 7, 187, 85);

            //bool test = false;

            //test = intake(patient_list[0], room_list[0]); // True
            //test = intake(patient_list[3], room_list[0]); // True
            //test = intake(patient_list[1], room_list[2]); // False

            //test = patient_intake(patient_list[3]); // True
            //test = patient_intake(patient_over_limit); // False

            //test = is_ready_to_leave(healthy_person); // True
            //test = is_ready_to_leave(patient_over_limit); // False

            //Console.WriteLine($"{test}\n");
            //room_list[0].get_info();

            patient_list[0].get_info();

            patient_list.Add(patient_over_limit); // Cannot be intaken -> weights more than all beds max weight
            patient_list.Add(healthy_person); // BMI in normal

            patients_intake(patient_list);

            is_ready_to_leave(healthy_person);

            Console.ReadKey();
        }

        public static bool intake(Patient new_patient, Room room)
        {
            if (new_patient.weight <= room.max_weight)
            {
                foreach (Bed bed in room.bed_list)
                {
                    if (bed.occupied == false)
                    {
                        bed.occupied = true;
                        bed.occupied_by = new_patient;
                        bed.occupied_by_id = new_patient.id;
                        break;
                    }
                }
                return true;
            }
            return false;
        }

        public static bool patient_intake(Patient new_patient)
        {
            foreach (Room room in room_list)
            {
                if (room.max_weight >= new_patient.weight)
                {
                    foreach(Bed bed in room.bed_list)
                    {
                        if (bed.occupied == false)
                        {
                            bed.occupied = true;
                            bed.occupied_by = new_patient;
                            bed.occupied_by_id = new_patient.id;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        
        public static void patients_intake(List<Patient> patients_to_intake)
        {
            List<Patient> sorted_patients_to_intake = patients_to_intake.OrderByDescending(p => p.weight).ToList();
            bool intaken;
            
            foreach (Patient patient in sorted_patients_to_intake)
            {
                intaken = patient_intake(patient);
                if (intaken == false) Console.WriteLine($"Patient {patient.first_name} {patient.last_name} cannot be intaken!\n");
                else Console.WriteLine($"Patient {patient.first_name} {patient.last_name} is intaken.\n");
            }
        }

        public static bool is_ready_to_leave(Patient patient) //TODO
        {
            if (patient.bmi < 25)
            {
                foreach(Room room in room_list)
                {
                    if (room.max_weight >= patient.weight)
                    {
                        foreach(Bed bed in room.bed_list)
                        {
                            if (bed.occupied_by_id == patient.id)
                            {
                                bed.occupied = false;
                                bed.occupied_by = null;
                                patient.currently_in_treatment = false;
                                break;
                            }
                        }
                    }
                }
                Console.WriteLine($"{patient.first_name} {patient.last_name} is cured    -> his/her bed is free now.");
                return true;
            }
            return false;
        }

    }

    public class Patient
    {
        public string first_name { get; private set; }
        public string last_name { get; private set; }
        public int id { get; private set; }
        public float weight { get; private set; }
        public float height { get; private set; }

        public float bmi { get; private set; }
        public bool currently_in_treatment { get; set; } = true;

        public Patient(string first_name, string last_name, int id, int height, int weight)
        {
            this.first_name = first_name;
            this.last_name = last_name;
            this.id = id;
            this.weight = weight;
            this.height = height;
            this.bmi = this.weight / (this.height / 100 * this.height / 100);        
        }

        public void weight_loss(int loss)
        {
            this.weight -= loss;
            this.bmi = this.weight / (this.height / 100 * this.height / 100);
        }

        public void weight_gain(int gain)
        {
            this.weight += gain;
            this.bmi = this.weight / (this.height / 100 * this.height / 100);
        }

        public void get_info()
        {
            Console.WriteLine($"Name: {this.first_name} {this.last_name}\nID: {this.id}\n" +
                $"Height: {this.height} | Weight: {this.weight}\nBMI: {this.bmi}\n" +
                $"Currently in Treatment: {this.currently_in_treatment}\n");
        }
        
    }

    public class Room
    {
        public int number_of_beds { get; private set; }
        public int max_weight { get; private set; }

        public List<Bed> bed_list { get; private set; } = new List<Bed>();

        public Room(int number_of_beds, int max_weight)
        {
            this.number_of_beds = number_of_beds;
            this.max_weight = max_weight;

            for (int i = 0; i < this.number_of_beds; i++) this.bed_list.Add(new Bed(this.max_weight));
        }

        public void get_info()
        {
            Console.WriteLine($"Free Beds: {this.free_beds()} of {this.number_of_beds}\n" +
                $"Beds Maximal Weight: {this.max_weight}\nPatients: \n");
            foreach (Bed bed in bed_list)
            {
                if (bed.occupied == true) bed.occupied_by.get_info();
            }
        }

        public int free_beds()
        {
            int counter = 0;
            foreach (Bed bed in bed_list) { if (bed.occupied == false) counter++; }        
            return counter;
        }
       
    }

    public class Bed
    {
        public int max_weight { get; private set; }

        public bool occupied { get;  set; } = false;
        public int occupied_by_id { get; set; } = 0;
        public Patient occupied_by { get; set; }
        

        public Bed(int max_weight) { this.max_weight = max_weight; }
    }

}