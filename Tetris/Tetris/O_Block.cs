﻿namespace Tetris
{
    public class O_Block : Block
    {
        public override int id => 4;
        protected override Position start_offset => new Position(0, 4);

        protected override Position[][] tiles => new Position[][]
        {
            new Position[] { new(0,0), new(0,1), new(1,0), new(1,1) }
        };
    }
}
