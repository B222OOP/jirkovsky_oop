﻿namespace Tetris
{
    public class Game_State
    {
        private Block Current_block;

        public Block current_block
        {
            get => Current_block;
            private set
            {
                Current_block = value;
                Current_block.reset();

                for (int i = 0; i < 2; i++)
                {
                    Current_block.move(1, 0);

                    if (!block_fits())
                    {
                        Current_block.move(-1, 0);
                    }
                }
            }
        }

        public Game_Grid game_grid { get; }
        public Block_Queue block_queue { get; }
        public bool game_over { get; private set; }
        public int score { get; private set; }

        public Game_State()
        {
            this.game_grid = new Game_Grid(22, 10);
            this.block_queue = new Block_Queue();
            current_block = block_queue.update();
        }

        private bool block_fits()
        {
            foreach (Position p in current_block.tile_positions())
            {
                if (!this.game_grid.is_empty(p.row, p.column))
                {
                    return false;
                }
            }

            return true;
        }


        public void rotate_block_cw()
        {
            current_block.rotate_cw();

            if (!block_fits())
            {
                current_block.rotate_ccw();
            }
        }

        public void rotate_block_ccw()
        {
            current_block.rotate_ccw();

            if (!block_fits())
            {
                current_block.rotate_cw();
            }
        }

        public void move_block_left()
        {
            current_block.move(0, -1);

            if (!block_fits())
            {
                current_block.move(0, 1);
            }
        }

        public void move_block_right()
        {
            current_block.move(0, 1);

            if (!block_fits())
            {
                current_block.move(0, -1);
            }
        }

        private bool is_game_over()
        {
            return !(this.game_grid.is_row_empty(0) && this.game_grid.is_row_empty(1));
        }

        private void place_block()
        {
            foreach (Position p in current_block.tile_positions())
            {
                this.game_grid[p.row, p.column] = current_block.id;
            }

            score += this.game_grid.clear_full_rows();

            if (is_game_over())
            {
                game_over = true;
            }
            else
            {
                current_block = this.block_queue.update();
            }
        }

        public void move_block_down()
        {
            current_block.move(1, 0);

            if (!block_fits())
            {
                current_block.move(-1, 0);
                place_block();
            }
        }
    }
}