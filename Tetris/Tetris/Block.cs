﻿using System.Collections.Generic;

namespace Tetris
{
    public abstract class Block
    {
        protected abstract Position[][] tiles { get; }
        protected abstract Position start_offset { get; }
        private Position offset;
        private int rotation_state;

        public abstract int id { get; }

        public Block()
        {
            offset = new Position(start_offset.row, start_offset.column);
        }

        public IEnumerable<Position> tile_positions()
        {
            foreach (Position p in tiles[rotation_state])
            {
                yield return new Position(p.row + offset.row, p.column + offset.column);
            }
        }

        public void rotate_cw()
        {
            rotation_state = (rotation_state + 1) % tiles.Length;
        }

        public void rotate_ccw()
        {
            if (rotation_state == 0) rotation_state = tiles.Length - 1;
            else rotation_state--;
        }

        public void move(int rows, int columns)
        {
            offset.row += rows;
            offset.column += columns;
        }

        public void reset()
        {
            rotation_state = 0;
            offset.row = start_offset.row;
            offset.column = start_offset.column;
        }
    }
}
