﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MySqlConnector;

namespace Tetris
{
    public partial class MainWindow : Window
    {
        private readonly ImageSource[] tile_images = new ImageSource[]
        {
            new BitmapImage(new Uri("Sources/empty_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/blue_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/cyan_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/green_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/orange_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/purple_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/red_tile.png", UriKind.Relative)),
            new BitmapImage(new Uri("Sources/yellow_tile.png", UriKind.Relative))
        };

        private readonly Image[,] image_control;
        private Game_State game_state = new Game_State();

        private DB_connect db_connection;
        private MySqlDataReader reader;

        public MainWindow()
        {
            InitializeComponent();
            image_control = SetupGameCanvas(game_state.game_grid);
        }

        private Image[,] SetupGameCanvas(Game_Grid grid)
        {
            Image[,] image_control = new Image[grid.rows, grid.colums];
            int cellSize = 25;

            for (int r = 0; r < grid.rows; r++)
            {
                for (int c = 0; c < grid.colums; c++)
                {
                    Image image = new Image
                    {
                        Width = cellSize,
                        Height = cellSize
                    };

                    Canvas.SetTop(image, (r - 2) * cellSize);
                    Canvas.SetLeft(image, c * cellSize);
                    game_canvas.Children.Add(image);
                    image_control[r, c] = image;
                }
            }
            return image_control;
        }

        private void draw_grid(Game_Grid grid)
        {
            for (int r = 0; r < grid.rows; r++)
            {
                for (int c = 0; c < grid.colums; c++)
                {
                    int id = grid[r, c];
                    image_control[r, c].Opacity = 1;
                    image_control[r, c].Source = tile_images[id];
                }
            }
        }

        private void draw_block(Block block)
        {
            foreach (Position p in block.tile_positions())
            {
                image_control[p.row, p.column].Opacity = 1;
                image_control[p.row, p.column].Source = tile_images[block.id];
            }
        }

        private void draw(Game_State game_state)
        {
            draw_grid(game_state.game_grid);
            draw_block(game_state.current_block);

            score_label.Content = $"Score: {game_state.score}";
        }

        private async Task game_loop()
        {
            draw(game_state);

            while (!game_state.game_over)
            {
                await Task.Delay(500);
                game_state.move_block_down();
                draw(game_state);
            }

            game_over_menu.Visibility = Visibility.Visible;
            final_score.Text = $"Score: {game_state.score}";
            leaderboard_refresh();
        }

        private void key_pressed(object sender, KeyEventArgs e)
        {
            if (game_state.game_over) return;

            switch (e.Key)
            {
                case Key.Left:
                    game_state.move_block_left();
                    break;
                case Key.Right:
                    game_state.move_block_right();
                    break;
                case Key.Down:
                    game_state.move_block_down();
                    break;
                case Key.Up:
                    game_state.rotate_block_cw();
                    break;
                case Key.Z:
                    game_state.rotate_block_ccw();
                    break;
                default:
                    return;
            }
            draw(game_state);
        }

        private async void game_canvas_loaded(object sender, RoutedEventArgs e)
        {
            await game_loop();
        }

        private async void play_again_clicked(object sender, RoutedEventArgs e)
        {
            game_state = new Game_State();
            game_over_menu.Visibility = Visibility.Hidden;
            await game_loop();
        }

        private void save_button_clicked(object sender, RoutedEventArgs e)
        {
            db_connection = new DB_connect();
            db_connection.Insert($"INSERT INTO leaderboard (score, player) VALUE ('{game_state.score}', '{player_textbox.Text}');");
            db_connection.CloseConn();
            leaderboard_refresh();
        }

        private void leaderboard_refresh()
        {
            db_connection = new DB_connect();
            List<string> score_list = new List<string>();
            string leaderboard_content = "Leaderboard: ";

            reader = db_connection.Select("SELECT score, player FROM leaderboard ORDER BY score DESC LIMIT 5;");
            while (reader.Read())
            {
                leaderboard_content += ($"\n{reader.GetInt32(0)} - {reader.GetString(1)}");
            }
            db_connection.CloseConn();
            leaderboard_texbox.Text = leaderboard_content;
        }
    }
}
