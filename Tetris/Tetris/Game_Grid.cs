﻿namespace Tetris
{
    public class Game_Grid
    {
        private readonly int[,] grid;
        public int rows { get; }
        public int colums { get; }

        public int this[int r, int c]
        {
            get => grid[r, c];
            set => grid[r, c] = value;
        }

        public Game_Grid(int rows, int columns)
        {
            this.rows = rows;
            colums = columns;
            grid = new int[rows, columns];
        }

        public bool is_inside(int r, int c)
        {
            return r >= 0 && r < rows
                && c >= 0 && c < colums;
        }

        public bool is_empty(int r, int c)
        {
            return is_inside(r, c) && grid[r, c] == 0;
        }

        public bool is_row_full(int r)
        {
            for (int c = 0; c < colums; c++)
            {
                if (grid[r, c] == 0) return false;
            }
            return true;
        }

        public bool is_row_empty(int r)
        {
            for (int c = 0; c < colums; c++)
            {
                if (grid[r, c] != 0) return false;
            }
            return true;
        }

        private void clear_row(int r)
        {
            for (int c = 0; c < colums; c++) grid[r, c] = 0;
        }

        private void move_row_down(int r, int numRows)
        {
            for (int c = 0; c < colums; c++)
            {
                grid[r + numRows, c] = grid[r, c];
                grid[r, c] = 0;
            }
        }

        public int clear_full_rows()
        {
            int cleared = 0;

            for (int r = rows - 1; r >= 0; r--)
            {
                if (is_row_full(r))
                {
                    clear_row(r);
                    cleared++;
                }
                else if (cleared > 0) move_row_down(r, cleared);
            }
            return cleared;
        }
    }
}
