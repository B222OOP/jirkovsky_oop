﻿using System;
using System.Windows.Documents;

namespace Tetris
{
    public class Block_Queue
    {
        private readonly Block[] blocks = new Block[]
        {
            new I_Block(), new J_Block(), new L_Block(),
            new O_Block(), new S_Block(), new T_Block(),
            new Z_Block()
        };

        private readonly Random random = new Random();

        public Block next_block { get; private set; }

        public Block_Queue()
        {
            next_block = random_block();
        }

        private Block random_block()
        {
            return blocks[random.Next(blocks.Length)];
        }

        public Block update()
        {
            Block block = next_block;

            do next_block = random_block();
            while (block.id == next_block.id);

            return block;
        }
    }
}
