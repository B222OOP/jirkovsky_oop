﻿namespace Tetris
{
    public class Z_Block : Block
    {
        public override int id => 1;
        protected override Position start_offset => new(0, 3);

        protected override Position[][] tiles => new Position[][] {
            new Position[] {new(0,0), new(0,1), new(1,1), new(1,2)},
            new Position[] {new(0,2), new(1,1), new(1,2), new(2,1)},
            new Position[] {new(1,0), new(1,1), new(2,1), new(2,2)},
            new Position[] {new(0,1), new(1,0), new(1,1), new(2,0)}
        };
    }
}
