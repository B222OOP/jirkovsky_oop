﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySqlConnector;

namespace test_3
{
    /// <summary>
    /// Interaction logic for CreateReservationWindow.xaml
    /// </summary>
    public partial class CreateReservationWindow : Window
    {
        public CreateReservationWindow()
        {
            InitializeComponent();
            DB_connect db_connection = new DB_connect();
            MySqlDataReader reader;
            List<User> user_list = new List<User>();
            List<Movie> movie_list = new List<Movie>();

            reader = db_connection.Select("SELECT u.user_id, u.first_name, u.last_name, r.role_name FROM user u JOIN role r ON role = role_id;");
            while (reader.Read())
            {
                user_list.Add(new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }

            reader = db_connection.Select("SELECT movie_id, name, genre, reserved FROM movie;");
            while (reader.Read())
            {
                movie_list.Add(new Movie(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }

            user_combobox.ItemsSource = user_list;
            movie_combobox.ItemsSource= movie_list;
            user_combobox.Items.Refresh();
            movie_combobox.Items.Refresh();

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void create_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection = new DB_connect();
            User user = (User)user_combobox.SelectedItem;
            Movie movie = (Movie)movie_combobox.SelectedItem;
            string insert = $"INSERT INTO reservation (user, movie, reserved_until) VALUES ('{user.id}', '{movie.id}', '{date_textbox.Text}');";
            db_connection.Insert(insert);
            db_connection.CloseConn();
            info_label.Content = "Reservation Created";
        }
    }
}
