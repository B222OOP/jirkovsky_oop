﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace test_3
{
    public class Movie
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string genre { get; private set; }
        //public int reserved { get; private set; }

        public Movie(int id,string name, string genre)
        {
            this.id = id;
            this.name = name;
            this.genre = genre;
            //this.reserved = reserved;
        }
    }
}
