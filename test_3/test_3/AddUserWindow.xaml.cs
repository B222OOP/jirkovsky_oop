﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace test_3
{
    /// <summary>
    /// Interaction logic for AddUserWindow.xaml
    /// </summary>
    public partial class AddUserWindow : Window
    {
        public AddUserWindow()
        {
            InitializeComponent();
        }

        private void create_button_Click(object sender, RoutedEventArgs e)
        {
            if (username_textbox.Text != null && password_1_passwordbox.Password == password_2_passwordbox.Password)
            {
                DB_connect db_connection;
                db_connection = new DB_connect();
                if (admin_checkbox.IsChecked == true)
                {
                    string insert = $"INSERT INTO user (first_name, last_name, username, password, role) VALUES ('{first_name_textbox.Text}', '{last_name_textbox.Text}', '{username_textbox.Text}', '{password_1_passwordbox.Password}', '1')";
                    db_connection.Insert(insert);
                }
                else
                {
                    string insert = $"INSERT INTO user (first_name, last_name, username, password, role) VALUES ('{first_name_textbox.Text}', '{last_name_textbox.Text}', '{username_textbox.Text}', '{password_1_passwordbox.Password}', '2')";
                    db_connection.Insert(insert);
                }
                error_label.Content = "User Created";
                db_connection.CloseConn();
            }
            else error_label.Content = "Passwords Do Not Match!";
        }
    }
}
