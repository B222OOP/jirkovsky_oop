﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_3
{
    public class Reservation
    {
        public int id { get; private set; }
        public string username { get; private set; }
        public string movie { get; private set; }
        public string name { get; private set; }
        //public string reserved_until { get; private set; }

        public Reservation(int id, string username, string movie)
        {
            this.id = id;
            this.username = username;
            this.movie = movie;
            this.name = $"{username} -> {movie}";

            //this.reserved_until = reserved_until;
        }
    }
}
