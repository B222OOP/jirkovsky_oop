﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace test_3
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        public int user_id { get; private set; }
        public UserWindow(int user_id)
        {
            InitializeComponent();
            select_refresh("SELECT movie_id, name, genre, reserved FROM movie WHERE reserved IS NULL;");
            this.user_id = user_id;
        }

        public void select_refresh(string select)
        {
            DB_connect db_connection;
            MySqlDataReader reader;
            db_connection = new DB_connect();
            List<Movie> movie_list = new List<Movie>();
            reader = db_connection.Select(select);
            while (reader.Read())
            {
                movie_list.Add(new Movie(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }
            db_connection.CloseConn();
            movie_listbox.ItemsSource = movie_list;
            movie_listbox.Items.Refresh();
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            select_refresh($"SELECT movie_id, name, genre FROM movie WHERE name LIKE '{search_textbox.Text}%'");
        }

        private void create_reservation_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection = new DB_connect();
            Movie movie = (Movie)movie_listbox.SelectedItem;
            db_connection.Insert($"Insert INTO reservation (user, movie, reserved_until) VALUES ('{this.user_id}', '{movie.id}', '{date_textbox.Text}');");
            db_connection.CloseConn();
            info_label.Content = "Reservation Created";
            select_refresh("SELECT movie_id, name, genre, reserved FROM movie WHERE reserved IS NULL;");
        }

        private void my_reservation_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection;
            MySqlDataReader reader;
            db_connection = new DB_connect();
            List<Reservation> reservation_list = new List<Reservation>();
            reader = db_connection.Select($"SELECT r.reservation_id, u.username, m.name, r.reserved_until FROM reservation r JOIN user u ON user = user_id JOIN movie m ON movie = movie_id WHERE r.user = {this.user_id};");
            while (reader.Read())
            {
                reservation_list.Add(new Reservation(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }
            db_connection.CloseConn();
            movie_listbox.ItemsSource = reservation_list;
            movie_listbox.Items.Refresh();
        }
    }
}
