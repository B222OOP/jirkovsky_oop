﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace test_3
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        private void users_refresh()
        {
            DB_connect db_connection;
            MySqlDataReader reader;
            db_connection = new DB_connect();
            List<User> user_list = new List<User>();
            reader = db_connection.Select("SELECT u.user_id, u.first_name, u.last_name, r.role_name  FROM user u JOIN role r ON role = role_id;");
            while (reader.Read())
            {
                user_list.Add(new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }
            db_connection.CloseConn();
            listbox.ItemsSource = user_list;
            listbox.Items.Refresh();
        }

        private void reservations_refresh()
        {
            DB_connect db_connection;
            MySqlDataReader reader;
            db_connection = new DB_connect();
            List<Reservation> reservation_list = new List<Reservation>();
            reader = db_connection.Select("SELECT r.reservation_id, u.username, m.name, r.reserved_until FROM reservation r JOIN user u ON user = user_id JOIN movie m ON movie = movie_id;");
            while (reader.Read())
            {
                reservation_list.Add(new Reservation(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }
            db_connection.CloseConn();
            listbox.ItemsSource = reservation_list;
            listbox.Items.Refresh();
        }


        private void movies_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection;
            MySqlDataReader reader;
            db_connection = new DB_connect();
            List<Movie> movie_list = new List<Movie>();
            reader = db_connection.Select("SELECT movie_id, name, genre, reserved FROM movie;");
            while (reader.Read())
            {
                movie_list.Add(new Movie(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }
            db_connection.CloseConn();
            listbox.ItemsSource = movie_list;
            listbox.Items.Refresh();
        }

        private void reservations_button_Click(object sender, RoutedEventArgs e)
        {
            reservations_refresh();
        }


        private void delete_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection = new DB_connect();

            try
            {
                User user = (User)listbox.SelectedItem;
                db_connection.Delete($"DELETE FROM user WHERE user_id = '{user.id}'");
                db_connection.CloseConn();
                users_refresh();
            } catch (System.InvalidCastException) {
                Reservation reservation = (Reservation)listbox.SelectedItem;
                db_connection.Delete($"DELETE FROM reservation WHERE reservation_id = {reservation.id}");
                db_connection.CloseConn();
                reservations_refresh();
            }

        }

        private void add_user_button_Click(object sender, RoutedEventArgs e)
        {
            AddUserWindow add_user_window = new AddUserWindow();
            add_user_window.ShowDialog();;
        }

        private void users_button_Click(object sender, RoutedEventArgs e)
        {
            users_refresh();
        }

        private void create_reservation_button_Click(object sender, RoutedEventArgs e)
        {
            CreateReservationWindow create_reservation = new CreateReservationWindow();
            create_reservation.ShowDialog();
        }
    }
}
