﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySqlConnector;

namespace test_3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        void login_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection;
            MySqlDataReader reader;
            db_connection = new DB_connect();
            reader = db_connection.Select("SELECT user_id, username, password, r.role_name FROM user JOIN role r ON role = r.role_id;");

            while (reader.Read())
            {
                int user_id = reader.GetInt32(0);
                string username = reader.GetString(1);
                string password = reader.GetString(2);
                string role = reader.GetString(3);

                if (username == username_textbox.Text && password == password_textbox.Password)
                {
                    if (role == "admin")
                    {
                        AdminWindow admin_window = new AdminWindow();
                        admin_window.Show();
                        this.Close();
                        break;
                    }
                    if (role == "customer")
                    {
                        UserWindow user_window = new UserWindow(user_id);
                        user_window.Show();
                        this.Close();
                        break;
                    }         
                }
            }
            info_label.Content = "Invalid Username or Password";
            db_connection.CloseConn();
        }
    }
}
