﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_3
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }

        public User(int id, string first_name, string last_name, string role)
        {
            this.id = id;
            this.name = $"{first_name} {last_name} - {role}";
        }
    }
}
